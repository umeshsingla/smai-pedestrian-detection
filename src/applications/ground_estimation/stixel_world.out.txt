2017-05-02 15:11:23 {7ff93b406b40} [ StereoCameraCalibration ] : stereo_calibration_data name: Andreas Ess Bahnhofstrasse sequence stereo calibration
2017-05-02 15:11:23 {7ff93b406b40} [ CpuPreprocessor ] : v1 ==   0.998839 0.00680083   0.047689
2017-05-02 15:11:23 {7ff93b406b40} [ CpuPreprocessor ] : R1 set with v1, v2, v3 ==
   0.998839  0.00680083    0.047689
-0.00692929    0.999973  0.00252875
 -0.0476705 -0.00285627    0.998859
2017-05-02 15:11:23 {7ff93b406b40} [ CpuPreprocessor ] : Final x_offset_left after centering == -28
2017-05-02 15:11:23 {7ff93b406b40} [ CpuPreprocessor ] : Final x_offset_right after centering == -34
2017-05-02 15:11:23 {7ff93b406b40} [ BaseGroundPlaneEstimator ] : left camera focal length x,y == 500.745, 500.323
2017-05-02 15:11:23 {7ff93b406b40} [ BaseGroundPlaneEstimator ] : right camera focal length x,y == 500.745, 500.323
2017-05-02 15:11:23 {7ff93b406b40} [ BaseApplication ] : Entering into main_loop
